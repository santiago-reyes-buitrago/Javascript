
const url = "https://www.balldontlie.io/api/v1/players";
let html;
const getData = (api) => {

    return fetch(api)
        .then((response) => response.json())
        .then((json) => {
                imprimirDatos(json)
                console.log(json)
        })
        .catch((error) => {
            console.log("Error: ",error)
        })

}
async function TraerJugador(ID)
{
    try {
        let response = await
            fetch(ID);
        let data = await response.json()
        return imprimirDatosespecificos(data);
    }catch (e){
        alert('No se ha encontrado el jugador');
    }

}
const imprimirDatosespecificos = (data) => {
    html = ""
    html += `<tr>`;
    html += `<td>${data.id}</td>`;
    html += `<td>${data.first_name}</td>`;
    html += `<td>${data.last_name}</td>`;
    html += `</tr>`;
    console.log(data);
    document.getElementById("Cuerpo").innerHTML = html;
}
let todadata;
const imprimirDatos = (data) => {
    todadata = data;
    let cabecera = "<tr><th>ID</th><th>Primer nombre</th><th>Ultimo nombre</th></tr>"
    html = ""
    data.data.forEach(Jugador => {
        html += `<tr>`;
        html += `<td>${Jugador.id}</td>`;
        html += `<td>${Jugador.first_name}</td>`;
        html += `<td>${Jugador.last_name}</td>`;
        html += `</tr>`;
    });
    document.getElementById("Cabecera").innerHTML = cabecera;
    document.getElementById("Cuerpo").innerHTML = html;
}
let Buscar = document.getElementById('Buscar');
Buscar.addEventListener('click', () =>{
    let Valor = document.getElementById('valor').value;
    let Busqueda = "https://balldontlie.io/api/v1/players/" + Valor + "";
    TraerJugador(Busqueda);

});
let tabla = document.getElementById('Reiniciar');
tabla.addEventListener('click', () => {
    getData(url);
});


getData(url);