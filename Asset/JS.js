console.log('* Corriendo *');

let personajes = [{
    nombre:'señor caballero gaiya',
    foto:'Gaiya.jpg',
    preguntas:['Es un guerrero ?',
        'Tiene un dragon ?',
        'Su ataque es de 2500 ?',
        'Quien lo utiliza ?',
        'Se puede invocar especialmente ?'],
    respuestas:['si','si','no','yugi muto','si']
},{
    nombre:'dragon blanco de ojos azules',
    foto: 'Dragonblanco.jpg',
    preguntas: ['Es un monstruo especial ?',
        'Es uan bestia alada ?',
        'Es de nivel 8 ?',
        'Quien lo utiliza ?',
        'se puede invocar normalmente ?'],
    respuestas: ['no','no','si','seto kaiba','si']
},{
    nombre:'kuriboh',
    foto: 'Kuriboh.jpg',
    preguntas: ['Es un monstruo normal ?',
        'tiene mucha defensa ?',
        'Es de rango 1 ?',
        'Quien lo utiliza ?',
        'se puede invocar normalmente ?'],
    respuestas: ['si','no','no','yugi muto','si']
},{
    nombre:'soldado del brillo negro',
    foto: 'Brillonegro.jpg',
    preguntas: ['Es un monstruo normal ?',
        'Su ataque es de 3000 ?',
        'Es de nivel 8 ?',
        'Quien lo utiliza ?',
        'se puede invocar normalmente ?'],
    respuestas: ['no','si','si','yugi muto','no']
},{
    nombre:'dragon de krystal',
    foto: 'Dragonkrystal.jpg',
    preguntas: ['Es un monstruo normal ?',
        'Su ataque es de 3000 ?',
        'es de tipo psiquico ?',
        'Quien lo utiliza ?',
        'se puede invocar normalmente ?'],
    respuestas: ['si','no','no','seto kaiba','si']
}];

const Jugar = document.getElementById('Playjuego');

let indice = 0;
console.log(indice);
let opacidad = 20;
let imgpersonaje = document.getElementById('imgPersonaje');
let imgpremio = document.getElementById('imgPremio');

Jugar.addEventListener('click', () =>{

    indice = Math.floor((Math.random() * (4 + 1)));
    console.log(indice);
    let pregunta0 = document.getElementById('pregunta0');
    let pregunta1 = document.getElementById('pregunta1');
    let pregunta2 = document.getElementById('pregunta2');
    let pregunta3 = document.getElementById('pregunta3');
    let pregunta4 = document.getElementById('pregunta4');

    imgpersonaje.src = './Asset/' + personajes[indice].foto;
    imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';

    pregunta0.textContent = personajes[indice].preguntas[0];
    pregunta1.textContent = personajes[indice].preguntas[1];
    pregunta2.textContent = personajes[indice].preguntas[2];
    pregunta3.textContent = personajes[indice].preguntas[3];
    pregunta4.textContent = personajes[indice].preguntas[4];

    let respuesta0 = document.getElementById('respuesta0');
    let respuesta1 = document.getElementById('respuesta1');
    let respuesta2 = document.getElementById('respuesta2');
    let respuesta3 = document.getElementById('respuesta3');
    let respuesta4 = document.getElementById('respuesta4');

    respuesta0.addEventListener('change', ()=>{
        if (respuesta0.value === personajes[indice].respuestas[0]){
            opacidad = opacidad - 2;
            imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';
            respuesta0.style.borderColor = 'blue';
            respuesta0.style.borderStyle = 'solid';
        }
        else {
            imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';
            respuesta0.style.borderColor = 'red';
            respuesta0.style.borderStyle = 'solid';
        }
        respuesta0.disabled = true;
    });

    respuesta1.addEventListener('change', ()=>{
        if (respuesta1.value === personajes[indice].respuestas[1]){
            opacidad = opacidad - 2;
            imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';
            respuesta1.style.borderColor = 'blue';
            respuesta1.style.borderStyle = 'solid';
        }
        else {
            imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';
            respuesta1.style.borderColor = 'red';
            respuesta1.style.borderStyle = 'solid';
        }
        respuesta1.disabled = true;
    });

    respuesta2.addEventListener('change', ()=>{
        if (respuesta2.value === personajes[indice].respuestas[2]){
            opacidad = opacidad - 2;
            imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';
            respuesta2.style.borderColor = 'blue';
            respuesta2.style.borderStyle = 'solid';
        }
        else {
            imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';
            respuesta2.style.borderColor = 'red';
            respuesta2.style.borderStyle = 'solid';
        }
        respuesta2.disabled = true;
    });

    respuesta3.addEventListener('change', ()=>{
        if (respuesta3.value === personajes[indice].respuestas[3]){
            opacidad = opacidad - 2;
            imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';
            respuesta3.style.borderColor = 'blue';
            respuesta3.style.borderStyle = 'solid';
        }
        else {
            imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';
            respuesta3.style.borderColor = 'red';
            respuesta3.style.borderStyle = 'solid';
        }
        respuesta3.disabled = true;
    });

    respuesta4.addEventListener('change', ()=>{
        if (respuesta4.value === personajes[indice].respuestas[4]){
            opacidad = opacidad - 2;
            imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';
            respuesta4.style.borderColor = 'blue';
            respuesta4.style.borderStyle = 'solid';
        }
        else {
            imgpersonaje.style.filter = 'blur(' + opacidad + 'px)';
            respuesta4.style.borderColor = 'red';
            respuesta4.style.borderStyle = 'solid';
        }
        respuesta4.disabled = true;
    });

    const btnrespuesta = document.getElementById('btnresponder');

    btnrespuesta.addEventListener('click', () => {

        const respuestageneral = document.getElementById('respuestageneral').value;
        console.log(" valor de indice: " + indice);

        console.log('Respuesta final: ' + respuestageneral.toLowerCase());
            if (respuestageneral.toLowerCase() === personajes[indice].nombre){
                imgpersonaje.style.filter = 'blur(0px)';
                imgpremio.src = './Asset/Ganar.jpg';
                alert('Ganaste');
                console.log('Re pro');
            }
            else {
                imgpremio.src = './Asset/Perdedor.jpg';
                imgpremio.style.width = '640px';
                imgpremio.style.height = '640px';
                alert('Perdiste');
                console.log('Loser');
            }
    });

});
